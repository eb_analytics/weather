import os
import pandas as pd
from ftplib import FTP
import boto3
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import datetime
import requests
from facebook_business.adobjects.adset import AdSet
from facebook_business.adobjects.campaign import Campaign
from facebook_business.adobjects.abstractobject import  AbstractObject
from facebook_business.api import FacebookAdsApi
import pandas as pd
import numpy as np
from IPython.core.display import display, HTML
import json
import pysftp

# wx globals
global t_delta_lower_threshold = 0
global t_delta_threshold = 0
global home = '/home/ubuntu/wx_market_batch'

# wx_market globals
global s_d_split = 0
global upper_limit = 0
global s_spend_u_limit = upper_limit*s_d_split
global s_spend_l_limit = 0
global d_spend_u_limit = upper_limit*(1-s_d_split)
global d_spend_l_limit = 0
global t_delta_upper_threshold = 0
global s_inflection = 0
global d_inflection = 0
global wx_frame = wx
global social_per_person_spend = s_spend_u_limit/wx_frame[wx_frame.day == 0].population.sum()* s_inflection
global display_per_person_spend = d_spend_u_limit/wx_frame[wx_frame.day == 0].population.sum()* d_inflection
global home='/home/ubuntu/wx_market_batch'
global aws_path = '/home/ubuntu/anaconda3/bin/aws'

# fb globals
access_token = 'token'
adset_id1 = 0
adset_id2 = 0
campaign_id = 0

# search for "password" or "goes here" to replace any hardcoded crendentials that were not made into globals

class wx:
    def __init__(self):
        pass

    def download_new_from_ftp(self):
        ftp = FTP('ftp.aes.accuweather.com')
        ftp.login(user='user', passwd = 'password')
        filename = 'daily-f.csv'
        localfile = open(home+'/'+filename, 'wb')
        ftp.retrbinary('RETR ' + filename, localfile.write, 1024)
        ftp.quit()
        localfile.close()

    def read_csv_and_merge(self):
        #Read in Weather Data
        weather=pd.read_csv('%s/daily-f.csv' % home,encoding = "ISO-8859-1")
        print(weather.shape)
        max_date = weather[weather['WEATHER STATION CODE'] == 'CYPY'][['DATA TYPE (O=Observed F=Forecast and N=Normal)','DATA DATE']]
        weather.drop('DATA TYPE (O=Observed F=Forecast and N=Normal)',axis=1,inplace=True)
        weather = weather.merge(max_date, on = 'DATA DATE')
        print(weather.shape)

        #Read in Zip Data. Filter to US only
        zip_map=pd.read_csv('%s/zip_codes.csv' % home,encoding = "ISO-8859-1")
        us_states = pd.read_csv('%s/us_states.csv' % home)
        zip_map = zip_map.merge(us_states,on='STATE')
        zip_map['ZIP CODE'] = zip_map['ZIP CODE'].astype('int')

        #Read in Zip tier.  Merge to zip data
        zip_dma = pd.read_csv('%s/zip_tiers.csv' % home)
        zip_map= zip_map.merge(zip_dma, on='ZIP CODE')
        zip_map.tier.fillna('Cold',inplace=True)

        #Read in DMA data.  Merge to zip data
        zip_dma = pd.read_csv('%s/dma.csv' % home)
        zip_dma.dma = zip_dma.dma.astype('int')
        zip_map= zip_map.merge(zip_dma, on='ZIP CODE')

        #Read in population data.  Merge to zip data
        zip_population=pd.read_csv('%s/zip_population.csv' % home)
        zip_map=zip_map.merge(zip_population,on='ZIP CODE')
        zip_map.fillna(0,inplace=True)

        #Read in email data.  Merge to zip data
        zip_email=pd.read_csv('%s/zip_email.csv' % home)
        zip_email = zip_email[['zipcode','EmailAddressHash_SHA256']].groupby('zipcode',as_index=False).count()
        zip_email = zip_email[zip_email.zipcode.apply(lambda x: x.isnumeric())].copy()
        zip_email.zipcode = zip_email.zipcode.astype('int')
        zip_email.rename(columns={'zipcode': 'ZIP CODE','EmailAddressHash_SHA256': 'email_count'},inplace=True)
        zip_map=zip_map.merge(zip_email,on='ZIP CODE',how='left')
        zip_map.fillna(0,inplace=True)

        #Calc current day
        current_day_data=weather.loc[weather['DATA TYPE (O=Observed F=Forecast and N=Normal)']=='M'].copy()
        current_day=current_day_data[['WEATHER STATION CODE','DATA DATE']]
        current_day=current_day.rename(index=str,columns={'DATA DATE': 'day_0'})
        weather=weather.merge(current_day,on='WEATHER STATION CODE')
        weather['DATA DATE'] = pd.to_datetime(weather['DATA DATE'])
        weather['day_0'] = pd.to_datetime(weather['day_0'])
        weather['day']=(weather['DATA DATE']-weather['day_0']).dt.days
        weather_zip=zip_map.merge(weather,on='WEATHER STATION CODE')

        weather_zip = weather_zip

    def map_column_names(self):
        field_map = {'ZIP CODE' : 'zip_code',        'WEATHER STATION CODE' : 'weather_station_code',        'STATE' : 'state',        'DATA TYPE (O=Observed F=Forecast and N=Normal)' : 'date_type',        'DATA DATE' : 'weather_date',        'HIGHEST TEMPERATURE °F' : 't_max',        'LOWEST TEMPERATURE °F' : 't_min',        'AVERAGE DAILY TEMPERATURE (All Hours) °F' : 't_avg',        'NORMAL HIGHEST TEMPERATURE °F' : 't_max_n',        'NORMAL LOWEST TEMPERATURE °F' : 't_min_n',        'NORMAL AVERAGE DAILY TEMPERATURE (All Hours) °F' : 't_avg_n',        'WARMEST HEAT INDEX °F' : 't_warmest',        'NORMAL WARMEST HEAT INDEX °F' : 't_warmest_n',        'COLDEST WIND CHILL TEMPERATURE °F' : 't_coldest',        'NORMAL COLDEST WIND CHILL TEMPERATURE °F' : 't_coldest_n',        'MAXIMUM "FEELS LIKE" TEMPERATURE °F' : 'trf_max',        'MINIMUM "FEELS LIKE" TEMPERATURE °F' : 'trf_min',        'DAILY AVERAGE "FEELS LIKE" TEMPERATURE (All Hours) °F' : 'trf_avg',        'NORMAL MAXIMUM "FEELS LIKE" TEMPERATURE °F' : 'trf_max_n',        'NORMAL MINIMUM "FEELS LIKE" TEMPERATURE °F' : 'trf_min_n',        'NORMAL DAILY AVERAGE "FEELS LIKE" TEMPERATURE (All Hours) °F' : 'trf_avg_n',        'AVERAGE DAILY WIND SPEED (All Hours M.P.H.)' : 'ws_avg',        'NORMAL AVERAGE DAILY WIND SPEED (All Hours  M.P.H.)' : 'ws_avg_n',        'AVERAGE DAILY CLOUD COVER (All Hours  Percent; %)' : 'cloud_avg',        'NORMAL AVERAGE DAILY CLOUD COVER (All Hours  Percent; %)' : 'cloud_avg_n',        'TOTAL MINUTES of SUNSHINE (WBI Calculated)' : 'sun_total',        'NORMAL TOTAL MINUTES of SUNSHINE (WBI Calculated)' : 'sun_total_n',        'TOTAL MINUTES of SUNSHINE POSSIBLE (on Cloud-free Day)' : 'sun_possible',        '10 YEAR NORMAL TOTAL DAILY WATER EQUIVALENT (Inches)' : 'precip_n',        'OBSERVED TOTAL DAILY WATER EQUIVALENT AWXCLIMO (Inches)' : 'precip',        'NORMAL SNOWFALL (WBI Calculated; Total of All Hours; Inches)' : 'snow_n',        'SNOWFALL (WBI FINAL AWXCLIMO; Inches)' : 'snow',        'day_0' : 'day_0',        'day' : 'day'}
        weather_zip.rename(columns=field_map, inplace=True)
        for a in weather_zip.columns.values:
            print('Mapped Column: %s' % a)

    def calc_wx_deltas(self):
        wx_vars = ['t_max','t_min','t_avg','t_warmest','t_coldest','trf_max',        'trf_min','trf_avg','ws_avg','cloud_avg','sun_total',        'precip','snow']
        for a in wx_vars:
            weather_zip[a+'_delta'] = weather_zip[a] - weather_zip[a+'_n']

    def calc_wx_threshold(self):
        weather_zip['zip_binary'] = weather_zip.t_max_delta < t_delta_threshold
        weather_zip['zip_sliding'] = weather_zip.t_max_delta < t_delta_lower_threshold
        weather_zip['population_on_binary'] = weather_zip[weather_zip.zip_binary].population.fillna(0)
        weather_zip['population_on_sliding'] = weather_zip[weather_zip.zip_sliding].population.fillna(0)
        weather_zip['t_delta'] = weather_zip.t_max_delta*weather_zip.population/(weather_zip[weather_zip.day == 0].population.sum())
        weather_zip['t_delta_pop'] = weather_zip.t_max_delta*weather_zip.population
        weather_zip['t_delta_pop_on_zip'] =  weather_zip[weather_zip.zip_binary].t_delta_pop

    def email_calcs(self):
        weather_zip['email_count_on'] = weather_zip[weather_zip.zip_binary].email_count.fillna(0)

    def dma_calcs(self):
        weather_dma = weather_zip[['dma','population','population_on_sliding','day','weather_date']].groupby(['dma','day','weather_date'],as_index=False).sum()
        weather_dma['percent_on'] = weather_dma.population_on_sliding/weather_dma.population
        weather_dma['dma_switch'] = weather_dma['percent_on'] > .5
        weather_zip = weather_zip.merge(weather_dma[['day','weather_date','dma','dma_switch']],on =['day','weather_date','dma'])
        weather_zip['population_on_dma'] =  weather_zip[ weather_zip.dma_switch].population
        weather_zip['t_delta_pop_on_dma'] =  weather_zip[ weather_zip.dma_switch].t_delta_pop

    def state_calcs(self):
        weather_state = weather_zip[['state','population','population_on_binary','day','weather_date']].groupby(['state','day','weather_date'],as_index=False).sum()
        weather_state['percent_on'] = weather_state.population_on_binary/weather_state.population
        weather_state['state_switch'] = weather_state['percent_on'] > .5
        weather_zip = weather_zip.merge(weather_state[['day','weather_date','state','state_switch']],on =['day','weather_date','state'])
        weather_zip['population_on_state'] =  weather_zip[ weather_zip.state_switch].population
        weather_zip['t_delta_pop_on_state'] =  weather_zip[ weather_zip.state_switch].t_delta_pop

        weather_zip['state_climate'] = ''
        weather_zip['state_climate_shift'] = ''
        for state in weather_zip.state.unique():
            tier_counts = weather_zip[weather_zip.state==state]['tier'].value_counts()
            weather_zip.loc[weather_zip.state == state, 'state_climate'] = tier_counts.index[0]

            tier_counts_shift = weather_zip[weather_zip.state==state]['tier_shift'].value_counts()
            weather_zip.loc[weather_zip.state == state, 'state_climate_shift'] = tier_counts_shift.index[0]

            print(state,' Mapped from ',tier_counts.index[0],' to ',tier_counts_shift.index[0])

    def sitespect_calcs(self):
        weather_zip['tier_shift'] = weather_zip.tier
        weather_zip.loc[(weather_zip.zip_binary & (weather_zip.tier == 'Mid')),'tier_shift'] = 'Cold'
        weather_zip.loc[(weather_zip.zip_binary & (weather_zip.tier == 'Hot')),'tier_shift'] = 'Mid'

class wx_market:
    def __init__(self,wx):
        pass

    def create_social_display_spends(self):
        wx_frame['t_delta_sliding_scale'] = wx_frame['t_max_delta']/t_delta_upper_threshold
        wx_frame.loc[wx_frame['t_delta_sliding_scale'] > 2,'t_delta_sliding_scale'] = 2
        wx_frame.loc[wx_frame['t_delta_sliding_scale'] < 0,'t_delta_sliding_scale'] = 0
        wx_frame['social_$'] = wx_frame['population_on_dma'] * social_per_person_spend *  wx_frame['t_delta_sliding_scale']
        wx_frame['display_$'] = wx_frame['population_on_dma'] * display_per_person_spend *  wx_frame['t_delta_sliding_scale']

    def create_html_for_email(self):
        weather_dma_day = wx_frame[['social_$','display_$','population','population_on_binary','population_on_dma','day','weather_date','t_delta','t_delta_pop_on_dma','t_delta_pop_on_zip','email_count_on']].groupby(['day','weather_date'],as_index=False).sum()


        # Calculate social/display spend
        weather_dma_day['social_%'] = weather_dma_day.population_on_dma / weather_dma_day.population
        weather_dma_day['display_%'] = weather_dma_day.population_on_dma / weather_dma_day.population
        weather_dma_day.loc[weather_dma_day['social_$'] < s_spend_l_limit,'social_$'] = 0
        weather_dma_day.loc[weather_dma_day['display_$'] < d_spend_l_limit,'display_$'] = 0


        # Calculate website
        weather_dma_day['eb.com_%'] = weather_dma_day.population_on_binary / weather_dma_day.population
        weather_dma_day['t_delta_zip_on'] =weather_dma_day['t_delta_pop_on_zip']/weather_dma_day['population_on_binary']

        weather_dma_day['social $'] = weather_dma_day['social_$'].map('${:,.0f}'.format)
        weather_dma_day['soc&dis %'] = (weather_dma_day['social_%']*100).map("{0:.1f}%".format)
        weather_dma_day['display $'] = weather_dma_day['display_$'].map('${:,.0f}'.format)
        weather_dma_day['display %'] = (weather_dma_day['display_%']*100).map("{0:.1f}%".format)
        weather_dma_day['eb.com %'] = (weather_dma_day['eb.com_%']*100).map("{0:.1f}%".format)
        weather_dma_day['actual_date'] = pd.to_datetime(weather_dma_day['weather_date'])
        weather_dma_day['weekday'] = weather_dma_day['actual_date'].dt.weekday_name
        weather_dma_day['US T Delta'] = (weather_dma_day['t_delta']).map("{0:+.1f}F".format)
        weather_dma_day['Mkt T Delta'] = (weather_dma_day['t_delta_zip_on']).map("{0:+.1f}F".format)
        weather_dma_day['Email'] = (weather_dma_day['email_count_on']/1000).map('{:,.0f}k'.format)

        output_cols = ['weather_date','weekday','social $','display $','soc&dis %','eb.com %','Email','US T Delta','Mkt T Delta']
        wx_html = weather_dma_day[weather_dma_day.day > -1][output_cols].head(30).to_html(index=False)

        day = datetime.datetime.today().day
        month = datetime.datetime.today().month
        year = datetime.datetime.today().year
        img_html = ''
        for i in range(0,4):
            weekday = (datetime.datetime.today() + datetime.timedelta(hours=i*24)).strftime("%A")
            link = 'http://maps1.pivotalweather.com/maps/models/gefs/%s%02d%02d00/0%s/sfct_anom.conus.png'
            img_html += '<h1>Temperature Anomaly for %s</h1><img src="%s" alt="Weather Map" />' % (weekday,link % (year,month,day,(i+1)*24))
        img_html += '<h1>Temperature Anomaly for 6-10 Days out</h1><img src="http://www.cpc.ncep.noaa.gov/products/predictions/610day/610temp.new.gif" alt="Weather Map" />'
        img_html += '<h1>Temperature Anomaly for 8-14 Days out</h1><img src="http://www.cpc.ncep.noaa.gov/products/predictions/814day/814temp.new.gif" alt="Weather Map" />'
        img_html += '<h1>Temperature Anomaly for 15-28 Days out</h1><img src="http://www.cpc.ncep.noaa.gov/products/predictions/WK34/gifs/WK34temp.gif" alt="Weather Map" />'

        BODY_HTML = """        <html>
        <head></head>
        <body>
        %s
        %s
        </body>
        </html>
        """ % (wx_html,img_html)

    def abstract_object_eq(self, other):
        return other is not None and hasattr(other, 'export_all_data') and             export_all_data() == other.export_all_data()

    def send_to_fb(self,day):
        try:
            print('Updating FB adsets')
            current_day = wx_frame[wx_frame.day == day]
            fb_spend = current_day['social_$'].sum()
            if fb_spend >  s_spend_u_limit:
                fb_spend =  s_spend_u_limit
            if fb_spend <  s_spend_l_limit:
                fb_spend = 0

            dmas = current_day[current_day.dma_switch].dma.unique()
            geo_markets = []
            for dma in dmas:
                geo_markets.append({'key': 'DMA:%s' % dma})

            AbstractObject.__eq__ = abstract_object_eq
            #where access token var was instantiated
            FacebookAdsApi.init(access_token=access_token)
            c = Campaign(campaign_id)
            c.update({Campaign.Field.daily_budget: int(fb_spend*100)})
            c.remote_update()

            ##### Ad 1 #####
            for elite_adset_id in [adset_id1,adset_id2]:
                adset = AdSet(fbid=elite_adset_id)
                adset.update({
                    #AdSet.Field.daily_budget: int(fb_spend*100/len(elite_adsets)),
                     AdSet.Field.targeting: {
                    'geo_locations': {
                            'geo_markets':
                                geo_markets,
                        },
                     'custom_audiences': [{'id': custom_audience_id}],
                     'excluded_custom_audiences': [{'id':excluded_cusom_audience_id}],
                    }
                })
                adset.remote_update()
                print('FB adset #1 updated')

            ##### Ad 2 Active Enthusiast#####
            for elite_adset_id in [adset_id3,adset_id4]:
                adset = AdSet(fbid=elite_adset_id)
                adset.update({
                    #AdSet.Field.daily_budget: int(fb_spend*100/len(elite_adsets)),
                     AdSet.Field.targeting: {
                    'geo_locations': {
                            'geo_markets':
                                geo_markets,
                        },
                    'age_max': 65,
                    'age_min': 18,
                    'excluded_custom_audiences': [{'id':excluded_custom_audience_id}],

                            'flexible_spec': [
                {
                    'interests': [
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        },
                        {
                            'id': '',
                            'name': ''
                        }
                    ]
                }
            ]

                    }
                })
                adset.remote_update()
                print('FB adset #2 updated')


        except Exception as e:
            print('ERROR: Facebook send Failed')
            print(e)

    def send_email(self):
        SENDER = "email@eddiebauer.com"
        RECIPIENT = ["emails"]
        AWS_REGION = "us-east-1"
        SUBJECT = "Weather Marketing Forecast"
        BODY_TEXT = "Hello,\r\nPlease see the below for the current weather marketing forecast."
        BODY_HTML = BODY_HTML

        CHARSET = "utf-8"
        client = boto3.client('ses',region_name=AWS_REGION)

        msg = MIMEMultipart('mixed')
        msg['Subject'] = SUBJECT
        msg['From'] = SENDER
        msg['To'] = ', '.join(RECIPIENT)

        msg_body = MIMEMultipart('alternative')

        textpart = MIMEText(BODY_TEXT.encode(CHARSET), 'plain', CHARSET)
        htmlpart = MIMEText(BODY_HTML.encode(CHARSET), 'html', CHARSET)

        msg_body.attach(textpart)
        msg_body.attach(htmlpart)
        msg.attach(msg_body)

        try:
            response = client.send_raw_email(
                Source=SENDER,
                Destinations=RECIPIENT,
                RawMessage={
                    'Data':msg.as_string(),
                },
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
        else:
            print("Weather Marketing Email sent. Message ID:"),
            print(response['MessageId'])

    def send_to_s3(self):
        try:
            print('Sending Data to S3')
            wx_frame.to_parquet("%s/current_weather.parquet" % home)
            os.system("%s s3 cp %s/current_weather.parquet s3://eb-inbox/accuweather_current/" % (aws_path,home))
            os.system("%s s3 cp %s/current_weather.parquet s3://eb-inbox/accuweather_history/%s.parquet" % (aws_path,home,str(datetime.date.today())))
            print('S3 Complete')
        except Exception as e:
            print('ERROR: Send to S3')
            print(e)

    def cheetah_ftp(self,file_path):
        srv = pysftp.Connection(host="ftp.eccmp.com", username="username",password="password")
        srv.cwd('/ToCheetah')
        srv.put(file_path)
        srv.close()
        #os.system('rm %s'  % file_path)

    def send_data_to_cheetah(self,day):
        try:
            print('Sending Data to Cheetah')
            zip_email=pd.read_csv('%s/zip_email_unhash.csv' % home,sep = '|',error_bad_lines=False,encoding = "ISO-8859-1",engine='python')
            zip_email.dropna(inplace=True)
            zip_email.rename(columns={'zipcode': 'zip_code'},inplace=True)
            zip_email_us = zip_email[((zip_email.country == '1.USA') & (zip_email.zip_code.apply(lambda x: x.isnumeric())))].copy()
            zip_email_ca = zip_email[zip_email.country == '2.CANADA'].copy()
            print(zip_email.shape)
            print(zip_email_us.shape)
            print(zip_email_ca.shape)
            zip_email_ca.zip_code = 0
            zip_email_us.zip_code = zip_email_us.zip_code.astype('int')
            zip_email_ca['zip_code'] = zip_email_ca.zip_code.astype('int')
            zip_email_ca['tier_shift'] = 'Cold'
            zip_email_ca['zip_binary'] = False
            current_day_zip = wx_frame[wx_frame.day == day].copy()
            date_series = pd.to_datetime(current_day_zip.weather_date)
            date_string = str(date_series.dt.year.max())+'_'+str(date_series.dt.month.max())+'_'+str(date_series.dt.day.max())
            current_day_zip=current_day_zip.merge(zip_email_us,on='zip_code')
            current_day_zip = current_day_zip[~current_day_zip.emailaddress.str.contains("'")]
            current_day_zip = current_day_zip[~current_day_zip.emailaddress.str.contains('"')]
            current_day_zip = current_day_zip[~current_day_zip.emailaddress.str.contains('-')]
            current_day_zip = current_day_zip[~current_day_zip.emailaddress.str.contains('-')]
            zip_email_ca = zip_email_ca[~zip_email_ca.emailaddress.str.contains("'")]
            zip_email_ca = zip_email_ca[~zip_email_ca.emailaddress.str.contains('"')]
            zip_email_ca = zip_email_ca[~zip_email_ca.emailaddress.str.contains('-')]
            zip_email_ca = zip_email_ca[~zip_email_ca.emailaddress.str.contains('-')]
            pd.concat([current_day_zip[['emailaddress','tier_shift','zip_binary','zip_code']]
                       ,zip_email_ca[['emailaddress','tier_shift','zip_binary','zip_code']]])\
                       .to_csv('%s/cheetah_weather_tiers_%s.csv' % (home,date_string),sep = '|',index=False)
            cheetah_ftp('%s/cheetah_weather_tiers_%s.csv' % (home,date_string))
            print('Cheetah Complete')
        except Exception as e:
            print('ERROR: Sending data to Cheetah')
            print(e)

    def send_tiers_to_site_spect(self,day):
        try:
            print('Running Site Spect Send')
            current_day = wx_frame[wx_frame.day == day].copy()
            for climate in [('Cold',717471,1971029),('Mid',717470,1971028),('Hot',717468,1971027)]:
                states = current_day[current_day.state_climate_shift == climate[0]].state.unique()
                geo_markets = []
                for state in states:
                    geo_markets.append('{ "Type": "Region", "Code": "US-%s" }' % state)
                geo_string = ','.join(geo_markets)
                url_conditions = '{"UrlCriterion": "^/(\\\?|$)","type": "Url"},{"BodyContentCriterion": "page_type\\\s*:\\\s*\\"Home\\"","type": "Body"}'

                match_string = '{"MatchConditions": [%s,{ "type": "GeoLocation", "GeoLocation": [%s]  }] }' % (url_conditions,geo_string)
                token = requests.post('https://admin1.sitespect.com/api/token', data = '{"apikey":"apikey token goes here"}').json()['token']
                add_states = requests.put('https://admin1.sitespect.com/api/site/348/factor/%s' % climate[1],headers={'X-API-TOKEN': token},data = match_string)
                add_states_reporting = requests.put('https://admin1.sitespect.com/api/site/348/responsepoint/%s' % climate[2],headers={'X-API-TOKEN': token},data = match_string)

                print('Response from Site Spect for %s:%s:%s' %(climate[0],climate[1],climate[2]))
                print(json.dumps(add_states_reporting.json(), indent=2))
                print(json.dumps(add_states.json(), indent=2))
        except Exception as e:
            print('ERROR: States to Site Spect')
            print(e)

    def send_to_ttd(self,day=0):
        current_day = wx_frame[wx_frame.day == day]
        display_metrics = current_day[current_day.dma_switch][['dma','display_$']].groupby(['dma'],as_index=False).sum()
        ttd_auth = json.dumps({"Login": "email@eddiebauer.com"
                           ,"Password": "password","TokenExpirationInMinutes": 30})
        token = requests.post('https://api.thetradedesk.com/v3/authentication'
                              ,headers = {'content-type':'application/json'}, data = ttd_auth).json()['Token']
        ad_groups = requests.post('https://api.thetradedesk.com/v3/adgroup/query/campaign'
                         ,headers = {'content-type':'application/json','TTD-Auth':token}
                         ,data=json.dumps({'CampaignId':'campaign id goes here','PageStartIndex':0,'PageSize':300})).json()
        for ad_group in ad_groups['Result']:
            display_spend = 0
            for i,row in display_metrics.iterrows():
                if ad_group['AdGroupName'].find(str(int(row['dma']))) > -1:
                    display_spend = int(row['display_$'])
                    turn_on=True
                    print('Turning on : %s with spend: $%s' % (ad_group['AdGroupName'],display_spend))
                    break
                else:
                    display_spend = 0
                    turn_on=False

            # ad_group_response = requests.get('https://api.thetradedesk.com/v3/adgroup/%s' % ad_group['AdGroupId']
            #                  ,headers = {'content-type':'application/json','TTD-Auth':token}).json()
            # ad_group_response['RTBAttributes']['BudgetSettings']['DailyBudget']['Amount'] = 10.0
            # ad_group_put = requests.put('https://api.thetradedesk.com/v3/adgroup'
            #                 ,headers = {'content-type':'application/json','TTD-Auth':token}
            #                 ,data=json.dumps(ad_group_response)
            #                 )


weather = wx()
#weather.download_new_from_ftp()
weather.read_csv_and_merge()
weather.map_column_names()
weather.calc_wx_deltas()
weather.calc_wx_threshold()
weather.email_calcs()
weather.dma_calcs()
weather.sitespect_calcs()
weather.state_calcs()



weather_market = wx_market(weather.weather_zip)
weather_market.create_social_display_spends()
#weather_market.send_to_fb(day=0)
weather_market.send_to_ttd(day=0)
#weather_market.send_data_to_cheetah(day=0)
#weather_market.send_tiers_to_site_spect(day=0)
#weather_market.create_html_for_email()
#display(HTML(weather_market.BODY_HTML))
#weather_market.send_email()
#weather_market.send_to_s3()

info = json.dumps({"Login": "email@eddiebauer.com","Password": "password","TokenExpirationInMinutes": 30})
token = requests.post('https://api.thetradedesk.com/v3/authentication',headers = {'content-type':'application/json'}, data = info).json()['Token']


a = requests.post('https://api.thetradedesk.com/v3/campaign/query/advertiser'
                 ,headers = {'content-type':'application/json','TTD-Auth':token}
                 ,data=json.dumps({'AdvertiserId':'advertiser id goes here','PageStartIndex':0,'PageSize':10}))
print(json.dumps(a.json(), indent=2))





ad_groups = requests.post('https://api.thetradedesk.com/v3/adgroup/query/campaign'
                 ,headers = {'content-type':'application/json','TTD-Auth':token}
                 ,data=json.dumps({'CampaignId':'campaign id goes here','PageStartIndex':0,'PageSize':300})).json()
for ad_group in ad_groups['Result']:
    print(ad_group['AdGroupId'],ad_group['AdGroupName'])
    ad_group_response = requests.get('https://api.thetradedesk.com/v3/adgroup/%s' % ad_group['AdGroupId']
                     ,headers = {'content-type':'application/json','TTD-Auth':token}).json()
    ad_group_response.pop('Availability', None)
    ad_group_response.pop('CreatedAtUTC', None)
    ad_group_response.pop('LastUpdatedAtUTC', None)
    ad_group_response['RTBAttributes']['BudgetSettings']['DailyBudget']['Amount'] = 10.0
    ad_group_put = requests.put('https://api.thetradedesk.com/v3/adgroup'
                    ,headers = {'content-type':'application/json','TTD-Auth':token}
                    ,data=json.dumps(ad_group_response)
                    )
    IsDefaultForDimension

print(a.json()['RTBAttributes']['BudgetSettings']['DailyBudget'])
json.dumps({'AdvertiserId':'advertiser id goes here','PageStartIndex':0,'PageSize':10})
ad_group_put

  "CreatedAtUTC": "2018-10-19T15:02:48.837",
  "LastUpdatedAtUTC": "2018-10-19T15:02:48.837",
ad_group_response['Availability']
print(ad_group_put)
